'use strict';

angular
  .module('qratorsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ng-token-auth',
    'ui.bootstrap'
  ])
  .factory('profileServer', function() {
    var profiles = {};
    var artists = [];
    var fans = [];

    function createArtists() {
      var artists = [];

      for ( var i = 0; i < 10; i++ ) {
              var profile = {};
              profile.id = i;
              profile.name = 'Profile ' + i;
              profile.photo = 'images/profile_photo_' + i + '.jpg';
              profile.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis odio sagittis varius convallis. Pellentesque justo sapien, laoreet sit amet felis eget, accumsan tempor tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis odio sagittis varius convallis. Pellentesque justo sapien, laoreet sit amet felis eget, accumsan tempor tortor.';
              profile.upvotes = Math.floor(Math.random() * 100) + 1;
              profile.downvotes = Math.floor(Math.random() * 100) + 1;
              profile.controversy = Math.floor((profile.upvotes + profile.downvotes) / 2);
              profile.rising = Math.floor((profile.upvotes + profile.downvotes) / profile.id);
              profile.youtubeLinks = [{name: 'Link1', link: 'https://www.youtube.com/watch?v=NhxM8seVcxE'}, {name: 'Link2', link: 'https://www.youtube.com/watch?v=_CL6n0FJZpk'}, {name: 'Link3', link: 'https://www.youtube.com/watch?v=724X5SWnskI'}];
              profile.soundcloudLinks = [{name: 'Link1', link: '245673410'}, {name: 'Link2', link: '259883503'}, {name: 'Link3', link: '206587218'}];
              profile.battlePoints = Math.floor(Math.random() * 100) + 1;

              artists.push(profile);
      }

      return artists;
    }

    function createFans() {
      var fans = [];

      for ( var i = 0; i < 10; i++ ) {
              var profile = {};
              profile.id = i;
              profile.name = 'Profile ' + i;
              profile.photo = 'images/profile_photo_' + i + '.jpg';
              profile.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis odio sagittis varius convallis. Pellentesque justo sapien, laoreet sit amet felis eget, accumsan tempor tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer facilisis odio sagittis varius convallis. Pellentesque justo sapien, laoreet sit amet felis eget, accumsan tempor tortor.';
              profile.upvotes = Math.floor(Math.random() * 100) + 1;
              profile.downvotes = Math.floor(Math.random() * 100) + 1;
              profile.controversy = Math.floor((profile.upvotes + profile.downvotes) / 2);
              profile.rising = Math.floor((profile.upvotes + profile.downvotes) / profile.id);
              profile.youtubeLinks = [{name: 'Link1', link: 'https://www.youtube.com/watch?v=NhxM8seVcxE'}, {name: 'Link2', link: 'https://www.youtube.com/watch?v=_CL6n0FJZpk'}, {name: 'Link3', link: 'https://www.youtube.com/watch?v=724X5SWnskI'}];
              profile.soundcloudLinks = [{name: 'Link1', link: 'https://soundcloud.com/dreamchasersrecords/chiraq-meek-mill-lil-durk-shy-glizzy-1'}, {name: 'Link2', link: 'https://soundcloud.com/speakerknockerz/speaker-knockerz-erica-kane-1'}, {name: 'Link3', link: 'https://soundcloud.com/jwattslive/speaker-knockerz-rico-story'}];
              profile.battlePoints = Math.floor(Math.random() * 100) + 1;
              
              fans.push(profile);
      }

      return fans;
    }

    artists = createArtists();
    fans = createFans();

    profiles.artists = artists;
    profiles.fans = fans;

    return profiles;
  })
  .factory('matchesGenerator', ['profileServer', function(profileServer) {
    
    var matches = [];
    var artists = profileServer.artists;
    var numOfArtists = artists.length;
    var numOfMatches = Math.floor(numOfArtists*2);

    function getRandomArtist(randomNumber) {
      var artist = artists[randomNumber];

      return artist;
    }

    function Match(host, away) {
      this.host = host;
      this.away = away;
    }

    function getRandomNumber(upToNumber) {
      var randomNumber = Math.floor(Math.random() * upToNumber + 1);

      return randomNumber;
    }

    function buildMatches() {
      var randomNumber = 0;
      var host = {};
      var away = {};

      for ( var index = 0; index < numOfMatches; index++ ) {
        
        do {
          randomNumber = getRandomNumber(numOfArtists - 1);
          host = getRandomArtist(randomNumber);
          randomNumber = getRandomNumber(numOfArtists - 1);
          away = getRandomArtist(randomNumber);
        } while (host.id === away.id);


        var newMatch = new Match(host, away);
        matches.push(newMatch);
      }

    }

    buildMatches();

    return matches;

  }])
  .filter('reverse', function() {
    return function(items) {
      return items.slice().reverse();
    };
  })
  .config(function ($routeProvider, $authProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/sign_out', {
        templateUrl: 'views/sign_out.html',
        controller: 'SignOutCtrl',
        controllerAs: 'signOut'
      })
      .when('/feed', {
        templateUrl: 'views/feed.html',
        controller: 'FeedCtrl',
        controllerAs: 'feed'
      })
      .when('/qrate', {
        templateUrl: 'views/qrate.html',
        controller: 'QrateCtrl',
        controllerAs: 'qrate'
      })
      .when('/battle', {
        templateUrl: 'views/battle.html',
        controller: 'BattleCtrl',
        controllerAs: 'battle'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);

    $authProvider.configure({
      apiUrl: 'http://giggazz.herokuapp.com'
    });
  })
  .run(['$rootScope', '$location', function($rootScope, $location) {
    $rootScope.$on('auth:login-success', function() {
      $location.path('/');
    });
    $rootScope.$on('auth:logout-success', function() {
      $location.path('/');
    });
  }]);