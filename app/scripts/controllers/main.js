'use strict';

angular.module('qratorsApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$interval', '$auth', '$location', function($scope, $rootScope, $interval, $auth, $location) {
    
    if ($rootScope.user.email) {
        $location.path('/feed');
    }

    $scope.loginForm = {};
    $scope.registrationForm = {};

    $scope.displayPasswordResetForm = false;
    $scope.passwordResetForm = {};

    $scope.showMessage = false;

    var backgroundPhotos = [
                            'mainback1.jpg',
                            'mainback2.jpg',
                            'mainback3.jpg',
                            'mainback4.jpg',
                            'mainback5.jpg'
                            ];

    var backgroundElement = document.querySelector('.main-background');
    var mainBackground = angular.element(backgroundElement);
    var currentBackgroundIndex = 1;
    var newImage = '';

    $interval(function () { $scope.rotateBackground(); }, 5000);

    $scope.rotateBackground = function() {

        newImage = 'url(\'images/' + backgroundPhotos[currentBackgroundIndex] + '\') top center no-repeat';
        mainBackground.css('background', newImage);
        mainBackground.css('background-size', '1600px 990px');
        
        if (currentBackgroundIndex === (backgroundPhotos.length -1)) {
            currentBackgroundIndex = 0;
        } else {
            currentBackgroundIndex += 1;
        }
    };

    $scope.handleSignInBtnClick = function() {
        $auth.submitLogin($scope.loginForm)
            .then(function(resp) {
                $scope.result = resp;
                $location.path('/feed');
            })
            .catch(function(resp) {
                $scope.test = true;
                $scope.result = resp.errors;
                $scope.alertType = 'danger';
            });
    };

    $scope.handleForgotPasswordLink = function() {
        $scope.displayPasswordResetForm = !$scope.displayPasswordResetForm;
    };

    $scope.handleRegisterBtnClick = function() {
        $auth.submitRegistration($scope.registrationForm)
            .then(function(resp) {
                handleRegisterResponse(resp);
            })
            .catch(function(resp){
                handleErrorResponse(resp);
            });
    };

    $scope.messageToggle = function() {
        $scope.showMessage = !$scope.showMessage;
    };

    $scope.sendPasswordReset = function() {
        $auth.requestPasswordReset($scope.passwordResetForm)
            .then(function(resp) {
                $scope.test = true;
                $scope.result = resp;
                $scope.alertType = 'info';
            })
            .catch(function(resp) {
                $scope.test = true;
                $scope.result = resp;
                $scope.alertType = 'danger';
            });
    };

    function handleErrorResponse(error) {
        switch (error.status) {
            case 422:
                $scope.infoMessage = error.statusText;
                $scope.showMessage = true;
                $scope.infoType = 'danger';
                break;
            default:
                $scope.infoMessage = error.statusText;
                $scope.showMessage = true;
                $scope.infoType = 'danger';
        }
        $scope.registrationForm = {};
    }

    function handleRegisterResponse(response) {
        $scope.infoType = 'info';
        switch (response.status) {
            case 200:
                $scope.infoMessage = 'We\'ve sent you an email. Please confirm your address to complete the registration process.';
                $scope.showMessage = true;
                break;
        }
        $scope.registrationForm = {};
    }

  }]);
