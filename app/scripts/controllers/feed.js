'use strict';

angular.module('qratorsApp')
    .controller('FeedCtrl', ['$scope', 'profileServer', '$uibModal', function($scope, profileServer, $uibModal) {

        $scope.profiles = profileServer.artists;
        $scope.fans = profileServer.fans;

        $scope.filters = {};
        $scope.filters.artists = 'rising';
        $scope.filters.fans = 'rising';

        $scope.refresh = function(type, filter) {
            switch (type) {
                case 'artists':
                    $scope.filters.artists = filter;
                    break;
                case 'fans':
                    $scope.filters.fans = filter;
                    break;
            }
        };

        $scope.openModal = function(size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/test_partial.html',
                controller: 'testPartialCtrl',
                size: size,
                resolve: {
                    testVar: function() {
                        return $scope.artists;
                    }
                }
            });

            modalInstance.result.then(function() {
                console.log('hit ok');
            }, function() {
                console.log('some error');
            });
        };

        $scope.upvoteProfile = function(profile) {
            $scope.profiles[profile.id].upvotes += 1;
        };

        $scope.downvoteProfile = function(profile) {
            $scope.profiles[profile.id].downvotes += 1;
        };

        $scope.upvoteFan = function(profile) {
            $scope.fans[profile.id].upvotes += 1;
        };

        $scope.downvoteFan = function(profile) {
            $scope.fans[profile.id].downvotes += 1;
        };

    }]);