'use strict';

/**
 * @ngdoc function
 * @name qratorsApp.controller:BattleCtrl
 * @description
 * # BattleCtrl
 * Controller of the qratorsApp
 */

angular.module('qratorsApp')
  .controller('BattleCtrl', ['$scope', 'profileServer', 'matchesGenerator', '$q', '$timeout', function ($scope, profileServer, matchesGenerator, $q, $timeout) {

    $scope.artists = profileServer.artists;
    $scope.fans = profileServer.fans;
    $scope.matches = matchesGenerator;
    $scope.currentMatch = 0;
    $scope.battleInProgress = true;
    $scope.battleHostWinner = false;
    $scope.battleAwayWinner = false;
    $scope.versusShown = true;

    $scope.awardBattlePoints = function(winner) {

        var type = 'host';

        if (winner) {
            type = 'away';
        }

        $scope.matches[$scope.currentMatch][type].battlePoints += 1;

        setWinner(type).then(function(result) {
            if (result === 'ok') {
                $scope.nextMatch();
            }
        });

    };

    function battlePlay() {
        var result = $q.defer();

        $scope.battleInProgress = true;

        $timeout(function() {
            result.resolve('ok');
        }, 500);

        return result.promise;

    }

    function toggleVersus(versusValue) {
        var result = $q.defer();

        $scope.versusShown = versusValue;


        result.resolve('ok');


        return result.promise;
    }

    function battleStop() {

        var result = $q.defer();

        $scope.battleInProgress = false;

        $timeout(function() {
            result.resolve('ok');
        }, 500);

        return result.promise;
    }

    $scope.nextMatch = function() {
        
        // reset winner-flicker class on both cards
        $scope.battleAwayWinner = false;
        $scope.battleHostWinner = false;

        // start leave animation
        toggleVersus(false)
            .then(function(result) {
                if (result === 'ok') {
                    return battleStop();
                }
            })
            .then(function(result) {
                if (result === 'ok') {
                    return setCurrentMatch(); // change cards to new match
                }
            })
            .then(function(result) {
                if (result === 'ok') {
                    return battlePlay(); // start enter animation
                }
            })
            .then(function(result) {
                if (result === 'ok') {
                    $timeout(function() {
                        toggleVersus(true);
                    }, 200); // display versus text
                }
            });

    };

    function setCurrentMatch() {
        var result = $q.defer();

        if ($scope.currentMatch < ($scope.matches.length - 1)) {
            $scope.currentMatch += 1;
        } else {
            $scope.currentMatch = 0;
        }

        $timeout(function() {
            result.resolve('ok');
        }, 500);

        return result.promise;
    }

    function setWinner(type) {

        var result = $q.defer();

        if (type === 'host') {
            $scope.battleHostWinner = true;
            $timeout(function() {
                result.resolve('ok');
            }, 700);
        } else {
            $scope.battleAwayWinner = true;
            $timeout(function() {
                result.resolve('ok');
            }, 700);
        }

        return result.promise;

    }

    // native xmlhttp request to a weather api service - just testing

    // function getWeather() {
    //     var weatherServiceUrl = 'http://api.openweathermap.org/data/2.5/weather?q=Belgrade,Srb&appid=b1b15e88fa797225412429c1c50c122a&apikey=2c8a5d3bece339499fc175eb146b33d9';
        
    //     var xhr = new window.XMLHttpRequest();
    //     xhr.open('GET', weatherServiceUrl, true);
        
    //     xhr.onreadystatechange = function() {
    //         if (xhr.readyState === 4) {
    //             if (xhr.status === 200) {
    //                 $scope.weatherData = xhr.responseText;
    //             }
    //         }
    //     };
        
    //     xhr.send();
    // }

    // getWeather();

  }]);
