'use strict';

angular.module('qratorsApp')
  .controller('SignOutCtrl', ['$scope', '$auth', function ($scope, $auth) {
    
    $auth.signOut()
      .then(
            // redirect to main view
        )
      .catch(function(resp) {
        $scope.message = resp;
      });

  }]);
