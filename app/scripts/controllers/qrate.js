'use strict';

angular.module('qratorsApp')
    .controller('QrateCtrl', ['$scope', 'profileServer', function($scope, profileServer) {

        $scope.profiles = profileServer.artists;

        $scope.currentProfileIndex = 0;

        $scope.setCurrentProfileIndex = function(index) {
            $scope.currentProfileIndex = index;
        };

        $scope.isCurrentProfileIndex = function(index) {
            return $scope.currentProfileIndex === index;
        };

        $scope.upvoteButtonClick = function(index) {
            $scope.profiles[index].upvotes += 1;

        };

        $scope.downvoteButtonClick = function(index) {
            $scope.profiles[index].downvotes += 1;
        };

        $scope.nextButtonClick = function() {
            if ($scope.currentProfileIndex < $scope.profiles.length - 1) {
                $scope.currentProfileIndex += 1;
            } else {
                $scope.currentProfileIndex = 0;
            }
        };

        $scope.previousButtonClick = function() {
            if ($scope.currentProfileIndex > 0) {
                $scope.currentProfileIndex -= 1;
            } else {
                $scope.currentProfileIndex = $scope.profiles.length - 1;
            }
        };

        // function buildIframe() {
        //     var soundcloudSource = 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/' + $scope.profiles[$scope.currentProfileIndex].soundcloudLinks.Link1 + '&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false';
        //     var iFrame           = getElementById('scPlayer');
        //     iFrame.src           = soundcloudSource;
        //     iFrame.width         = '100%';
        //     iFrame.height        = '166';
        //     iFrame.scrolling     = 'no';
        //     iFrame.frameborder   = 'no';
        // }

        
        // buildIframe();
    }]);